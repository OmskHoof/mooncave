$(window).load(function(){
	$(".gall img").click(function(){
		$(this).attr("class", "show");
		var a = $(this).attr("src");
		a = a.substr(0, a.length - 4);
		var h = $("#fullphoto").height();
		a = a + h;
		$("#fullphoto").html('<img src="' + a + '" alt="" />');
		$("#fullphoto, .next, .prev, .close, .strl, .strr").fadeIn(500);
		$("#fullphoto img").fadeIn(2000);
		$.get(a, {nw:1}, function(w){
			$("#fullphoto img").css("margin-left", "-"+w/2+"px");
		});
	});
	$(".next").click(function(){
		if($(".gall img.show").next("img").length>0) {
			$(".gall img.show").next("img").attr("class", "show");
			$(".gall img.show:first").attr("class", "");
			var a = $(".gall img.show").attr("src");
			a = a.substr(0, a.length - 4);
			var h = $("#fullphoto").height();
			a = a + h;
			$("#fullphoto").html('<img src="' + a + '" alt="" />');
			$.get(a, {nw:1}, function(w){
				$("#fullphoto img").css("margin-left", "-"+w/2+"px");
			});
		}
	});
	$(".prev").click(function(){
		if($(".gall img.show").prev("img").length>0) {
			$(".gall img.show").prev("img").attr("class", "show");
			$(".gall img.show:last").attr("class", "");
			var a = $(".gall img.show").attr("src");
			a = a.substr(0, a.length - 4);
			var h = $("#fullphoto").height();
			a = a + h;
			$("#fullphoto").html('<img src="' + a + '" alt="" />');
			$.get(a, {nw:1}, function(w){
				$("#fullphoto img").css("margin-left", "-"+w/2+"px");
			});
		}
	});
	$(".close").click(function(){
		$("#fullphoto, .next, .prev, .close, .strl, .strr").fadeOut(300);
		$(".gall img").attr("class", "");
	});
});
