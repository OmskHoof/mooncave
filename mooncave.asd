(defsystem :mooncave
  :author "Mark Fedurin <hitecnologys@gmail.com>"
  :description "Базовый движок для модулей."
  :depends-on (:alexandria
               :restas
               :cl-store)
  :pathname "src/core/"
  :components ((:file "packages")
               (:file "resources")
               (:file "decorators")))

(defsystem :mooncave/music
  :author "Mark Fedurin <hitecnologys@gmail.com>"
  :description "Модуль, отвечающий за загрузку, кэширование и выдачу музыки для вещания."
  :long-description "Модулю предполагается выполнять следующее:
* Загрузка музыки из заданого плейлиста в кэш по необходимости
* Предоставление интерфейса управления плейлистами
* Выдача музыки для вещания liquidsoap'у по требованию
* Выдача метадаты веб-плееру"
  :defsystem-depends-on (:closure-template)
  :depends-on (:mooncave
               :yason
               :cxml
               :drakma
               :local-time
               :cl-ppcre
               :hctsmsl)
  :pathname "src/modules/music/"
  :components ((:module "templates"
                        :components ((:closure-template "control-center")))
               (:file "packages"
                      :depends-on ("templates"))
               (:file "utilities"
                      :depends-on ("packages"))
               (:file "playlist"
                      :depends-on ("utilities"))
               (:file "cache"
                      :depends-on ("playlist"))
               (:file "apis"
                      :depends-on ("cache"))
               (:file "api-secrets"
                      :depends-on ("apis"))
               (:file "icecast"
                      :depends-on ("apis"))
               (:file "lastfm"
                      :depends-on ("apis"))
               (:file "jamendo"
                      :depends-on ("apis"))
               (:file "magnatune"
                      :depends-on ("apis"))
               (:file "pleer"
                      :depends-on ("apis"))
               (:file "vk"
                      :depends-on ("apis"))
               (:file "music"
                      :depends-on ("icecast"
                                   "lastfm"
                                   "jamendo"
                                   "magnatune"
                                   "pleer"
                                   "vk"))))

(defsystem :mooncave/metadata-updater
  :author "Mark Fedurin <hitecnologys@gmail.com>"
  :description "Обновлялка обложек и названий. Берёт их с Icecast'а."
  :depends-on (:mooncave
               :mooncave/music
               :drakma)
  :serial t
  :pathname "src/modules/metadata-updater/"
  :components ((:file "packages")
               (:file "metadata")
               (:file "routes")
               (:file "timers")))

(defsystem :mooncave/website
  :author "Mark Fedurin <hitecnologys@gmail.com>"
  :description "Вебсайт. Десктопная и мобильная версии."
  :defsystem-depends-on (:closure-template)
  :depends-on (:mooncave)
  :serial t
  :pathname "src/modules/website/"
  :components ((:module "templates"
                        :serial t
                        :components ((:module "html"
                                              :components ((:closure-template "index")
                                                           (:closure-template "menu")
                                                           (:closure-template "chat")
                                                           (:closure-template "contacts")
                                                           (:closure-template "stuff")
                                                           (:closure-template "mobile")))
                                     (:module "js"
                                              :components ((:closure-template "song-metadata-updater")
                                                           (:closure-template "jplayer")))))
               (:file "packages")
               (:file "static")))
