(in-package :mooncave)

(defparameter *static-dir* #p"static/")
(defparameter *resource-dir* #p"res/")

;;; **************************************************************************
;;;  Пути
;;; **************************************************************************

(defun relative-pathname (base rel)
  (merge-pathnames rel
                   (merge-pathnames base
                                    (asdf:system-source-directory :mooncave))))

;;; **************************************************************************
;;;  Статика
;;; **************************************************************************

(defun static-file (pathname)
  (relative-pathname *static-dir* pathname))

;;; **************************************************************************
;;;  Прочие ресурсы
;;; **************************************************************************

(defun resource (pathname)
  (ensure-directories-exist (merge-pathnames *resource-dir*
                                             (asdf:system-source-directory :mooncave))
                            :mode 511)
  (relative-pathname *resource-dir* pathname))

;;; **************************************************************************
;;;  Сохранение/Загрузка ресурсов
;;; **************************************************************************

(defun make-pathname-from-id (id)
  (make-pathname :name id :type "dat"))

(defun save-data (data id)
  (cl-store:store data (resource (make-pathname-from-id id))))

(defun load-data (id)
  (when-let (pathname (probe-file (resource (make-pathname-from-id id))))
    (cl-store:restore pathname)))
