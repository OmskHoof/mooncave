(restas:define-module :mooncave
  (:use :cl
        :alexandria)
  (:export :*hostname*

           :*static-dir*
           :*resource-dir*
           :static-file
           :resource
           :save-data
           :load-data

           :@no-cache))

(in-package :mooncave)

(defparameter *hostname* "mooncave.net")
