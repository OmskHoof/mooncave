(in-package :mooncave)

;;; **************************************************************************
;;;  Контроль кэширования
;;; **************************************************************************

(defclass no-cache-route (routes:proxy-route) ())

(defmethod restas:process-route :before ((route no-cache-route) bindings)
  (setf (hunchentoot:header-out :expires)
        (hunchentoot:rfc-1123-date))
  (setf (hunchentoot:header-out :cache-control)
        "max-age=0, no-store, no-cache, must-revalidate"))

(defun @no-cache (route)
  (make-instance 'no-cache-route :target route))
