(in-package :mooncave.website)

;;; **************************************************************************
;;;  robots.txt
;;; **************************************************************************

(restas:define-route robots.txt ("robots.txt")
  (static-file #p"other/robots.txt"))

;;; **************************************************************************
;;;  Десктопная версия
;;; **************************************************************************

(in-package :mooncave.website.desktop)

;; ----------------
;; Прочее

(restas:define-route radio.m3u ("other/radio.m3u")
  (static-file #p"other/radio.m3u"))

;; ----------------
;; JS

(restas:define-route backstretch.min.js ("js/backstretch")
  (static-file #p"js/backstretch.min.js"))

(restas:define-route front.js ("js/front")
  (concatenate 'string
               (mooncave.js:update-metadata
                 (list :song-title-url (restas:genurl 'mooncave.metadata-updater:track-title)
                       :song-cover-url (restas:genurl 'mooncave.metadata-updater:track-cover)))
               (mooncave.js:jplayer
                 (list :host *hostname*))))

(restas:define-route gallery.js ("js/gallery")
  (static-file #p"js/gallery.js"))

(restas:define-route jquery-latest.js ("js/jquery")
  (static-file #p"js/jquery-latest.js"))

(restas:define-route jquery-ui.js ("js/jquery-ui")
  (static-file #p"js/jquery-ui.js"))

(restas:define-route jplayer.js ("js/jplayer")
  (static-file #p"js/jp/jplayer.js"))

(restas:define-route jplayerold.js ("js/jplayer-old")
  (static-file #p"js/jp/jplayerold.js"))

(restas:define-route Jplayer.swf ("swf/jplayer/Jplayer.swf")
  (static-file #p"js/jp/Jplayer.swf"))

;; ----------------
;; CSS

(restas:define-route desktop-style ("css/desktop")
  (static-file #p"css/desktop.css"))

;; ----------------
;; Картинки

(restas:define-route favicon ("img/favicon" :content-type "image/x-icon")
  (static-file #p"img/favicon.ico"))

(restas:define-route blur.svg ("img/blur" :content-type "image/svg")
  (static-file #p"img/blur.svg"))

(restas:define-route loading.gif ("img/loading" :content-type "image/gif")
  (static-file #p"img/loading.gif"))

(restas:define-route derp.gif ("img/derp" :content-type "image/gif")
  (static-file #p"img/derp.gif"))

;; ----------------
;; Страницы

(restas:define-route stuff ("stuff")
  (:render-method #'mooncave.desktop.pages:stuff)
  (list :favicon (restas:genurl 'favicon)
        :stylesheet (restas:genurl 'desktop-style)
        :derp-img (restas:genurl 'derp.gif)))

(restas:define-route menu ("menu")
  (:render-method #'mooncave.desktop.pages:menu)
  (list :stylesheet (restas:genurl 'desktop-style)
        :hostname *hostname*
        :stuff-url (restas:genurl 'stuff)
        :jquery (restas:genurl 'jquery-latest.js)
        :jquery-ui (restas:genurl 'jquery-ui.js)
        :backstretch (restas:genurl 'backstretch.min.js)
        :jplayer (restas:genurl 'jplayer.js)
        :front (restas:genurl 'front.js)))

(restas:define-route chat ("chat")
  (:render-method #'mooncave.desktop.pages:chat)
  (list :stylesheet (restas:genurl 'desktop-style)))

(restas:define-route desktop-home ("")
  (:render-method #'mooncave.desktop.pages:main-page)
  (list :favicon (restas:genurl 'favicon)
        :stylesheet (restas:genurl 'desktop-style)
        :menu (restas:genurl 'menu)
        :chat (restas:genurl 'chat)))

;;; **************************************************************************
;;;  Мобильная версия
;;; **************************************************************************

(in-package :mooncave.website.mobile)

;; ----------------
;; Прочее

(restas:define-route radio.m3u ("other/radio.m3u")
  (static-file #p"other/radio.m3u"))

;; ----------------
;; Картинки

(restas:define-route favicon ("img/favicon.ico" :content-type "image/x-icon")
  (static-file #p"img/favicon.ico"))

;; ----------------
;; CSS

(restas:define-route mobile-style ("css/mobile.css")
  (static-file #p"css/mobile.css"))

;; ----------------
;; JS

(restas:define-route jquery-latest.js ("js/jquery-latest.js")
  (static-file #p"js/jquery-latest.js"))

(restas:define-route song-metadata-updater.js ("js/song-metadata-updater.js")
  (:render-method #'mooncave.js:update-metadata)
  (list :song-title-url (restas:genurl 'mooncave.metadata-updater.track-title)
        :song-cover-url (restas:genurl 'mooncave.metadata-updater.track-cover)))

;; ----------------
;; Страницы

(restas:define-route mobile-home ("")
  (:render-method #'mooncave.mobile.pages:mobile-page)
  (let* ((high-bitrate? (when (equal "1" (hunchentoot:get-parameter "high-quality"))
                          t)))
    (list :favicon "/img/favicon.ico"
          :stylesheet "/css/mobile.css"
          :radio-source (if high-bitrate?
                          "http://mooncave.net:8000/radio"
                          "http://mooncave.net:8000/radio64")
          :bit-rate-change-url (restas:genurl 'mobile-home
                                              :high-quality (if high-bitrate?
                                                              "0"
                                                              "1"))
          :bit-rate-change-text (if high-bitrate?
                                  "Переключить на 64 Кбит/с"
                                  "Переключить на 256 Кбит/с")
          :current-bit-rate (if high-bitrate?
                              "256 Кбит/с"
                              "64 Кбит/с")
          :jquery "/js/jquery-latest.js"
          :song-updater "/js/song-metadata-updater.js")))
