(restas:define-module :mooncave.website
  (:use :cl
        :alexandria
        :mooncave))

(restas:define-module :mooncave.website.desktop
  (:use :cl
        :alexandria
        :mooncave))

(restas:define-module :mooncave.website.mobile
  (:use :cl
        :alexandria
        :mooncave))
