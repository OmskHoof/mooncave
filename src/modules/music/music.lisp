(in-package :mooncave.music)

;;; **************************************************************************
;;;  Инициализация
;;; **************************************************************************

(defmethod restas:initialize-module-instance :before ((module (eql #.*package*)) context)
  (declare (ignore context))
  (load-playlists)
  (load-timetable)
  (start-scheduler))

;;; **************************************************************************
;;;  Рендеринг
;;; **************************************************************************

(defun make-playlist-control-toolbar ()
  (<:div '(:class "playlist-control-toolbar")
         (<:a (list :class "delete-selected-tracks")
              "Удолить")
         (<:a (list :class "save-selected-tracks")
              "Схоронить")
         (<:a (list :class "move-selected-tracks-up")
              "Выше")
         (<:a (list :class "move-selected-tracks-down")
              "Ниже")))

(defun make-track-control-toolbar (track-id)
  (<:div '(:class "track-control-toolbar")
         (<:a (list :class "delete-track" :data-track-id track-id)
              "❌")
         (<:a (list :class "save-track" :data-track-id track-id)
              "💾")
         (<:a (list :class "move-track-up" :data-track-id track-id)
              "↑")
         (<:a (list :class "move-track-down" :data-track-id track-id)
              "↓")))

(defun render-quality (quality)
  (destructuring-bind (type . value) quality
    (format nil "~A: ~A"
            type
            (ecase type
              (:mp3
                (if (equalp "VBR" value)
                  value
                  (format nil "~D Кбит/с" value)))))))

(defun render-playlist (playlist)
  (with-playlist playlist (tracks current-track-position)
    (loop for track in tracks
          for i from 0
          collecting
          (with-slots (id title artist length quality service) track
            (<:tr (if (not (= i current-track-position))
                    '(:class "track")
                    '(:class ("track" "current")))
                  (<:td
                    (<:input (list :type "checkbox"
                                   :class "track-select"
                                   :data-track-id id)))
                  (<:td title)
                  (<:td artist)
                  (<:td (encode-time length))
                  (<:td (render-quality quality))
                  (<:td service)
                  (<:td/block
                    (make-track-control-toolbar id)))))))

(defun render-playlist-selector ()
  (apply #'<:ul '(:class "playlists")
         (loop for playlist in *playlists*
               for id = (playlist-id playlist)
               collecting (<:li '(:class "playlist")
                                (<:a (if (equalp (playlist-id *current-playlist*)
                                                 (playlist-id playlist))
                                       (list :data-id id
                                             :id (format nil "playlist-~D" id)
                                             :class '("view-playlist" "current" "viewing"))
                                       (list :data-id id
                                             :id (format nil "playlist-~D" id)
                                             :class "view-playlist"))
                                     id)
                                (<:a (list :class "change-playlist"
                                           :data-id id)
                                     "[✔]")))))

;;; **************************************************************************
;;;  Маршруты
;;; **************************************************************************

;; ----------------
;; Стили

(restas:define-route stylesheet ("css/stylesheet" :content-type "text/css")
  (hctsmsl:css
    '((:element "body")
      (:background-color "#EEEEEE"))
    '((:element "a")
      (:color "#0000FF"))
    '((:pseudoclass (:a "hover"))
      (:text-decoration "underline"))
    ;; ----------------
    '((:id "playlist-control")
      (:text-align "center")
      (:margin-top "15pt"))
    ;; ----------------
    '((:class "request-status")
      (:width "100%")
      (:margin "0 auto")
      (:margin-bottom "5pt")
      (:text-align "center")
      (:font-size "120%")
      (:position "absolute"))
    '((:parent (:class "span" "pending"))
      (:color "#1111AA"))
    '((:parent (:class "span" "success"))
      (:color "#11AA11"))
    '((:parent (:class "span" "failure"))
      (:color "#AA1111"))
    ;; ----------------
    '((:class "current")
      (:color "#DD1111"))
    '((:class "viewing")
      (:text-decoration "underline"))
    ;; ----------------
    '((:class "playlists")
      (:list-style "none")
      (:text-align "center"))
    '((:child (:class "" "playlists") "li")
      (:display "inline")
      (:margin-left "5pt")
      (:margin-right "5pt")
      (:border "solid 2px #AAAAAA")
      (:padding "3pt"))
    ;; ----------------
    '((:class "track-addition")
      (:width "80%")
      (:margin-left "auto")
      (:margin-right "auto"))
    '((:child (:class "" "track-addition") "input")
      (:width "80%"))
    ;; ----------------
    '((:class "tracks-list")
      (:border "solid 1px #666666")
      (:width "80%")
      (:text-align "center")
      (:margin-left "auto")
      (:margin-right "auto"))
    '((:child (:class "" "tracks-list") "tr" "td")
      (:border "solid 1px #666666"))
    ;; ----------------
    '((:class "playlist-control-toolbar")
      (:width "80%")
      (:margin-left "auto")
      (:margin-right "auto")
      (:margin-bottom "5pt"))
    '((:child (:class "div" "top-bar") (:class "div" "playlist-control-toolbar"))
      (:border "solid 1px #666666")
      (:margin-top "30pt"))))

;; ----------------
;; Скрипты

(restas:define-route javascript ("js/playlist-control" :content-type "text/javascript")
  (:render-method #'mooncave.music.js:playlist-control)
  (list :api-url "/music/playlist"))

(restas:define-route jquery ("js/jquery" :content-type "text/javascript")
  (static-file "js/jquery-latest.js"))

;; ----------------
;; API

(restas:define-route api/current-track ("music/track")
  (get-cached-track))

(restas:define-route api/next-track ("music/next-track")
  (prog1 (get-cached-track)
    (signal-event 'Next-track)))

(restas:define-route api/current-playlist ("music/playlist/current-playlist")
  (playlist-id (current-playlist)))

;; ----------------
;; Методы редактора

(defun add-track (title playlist)
  (when playlist
    (let ((tracks (mooncave.music.pleer:search-tracks title)))
      (unless (emptyp tracks)
        (with-playlist playlist (items position)
          (push (first tracks) items))
        (next-track playlist)
        t))))

;;; TODO Добавить проверку на существование трека
(defun delete-track (id playlist)
  (when playlist
    (with-playlist playlist (items position)
      (setf items (remove-if (curry #'track-id= id) items
                             :key #'track-id))
      t)))

(defun move-track (id delta playlist)
  (when playlist
    (with-playlist playlist (items position)
      (let* ((length (length items))
             (i (mod (position id items
                               :test #'track-id=
                               :key #'track-id)
                     length))
             (j (mod (+ i delta)
                     length)))
        (rotatef (nth i items)
                 (nth j items)))
      t)))

(restas:define-route playlist-editor/add ("music/playlist/add" :method :post)
  (:additional-variables (title (hunchentoot:post-parameter "title"))
                         (playlist (hunchentoot:post-parameter "playlist")))
  (prog1
    (if (add-track title (get-playlist playlist))
      hunchentoot:+http-ok+
      hunchentoot:+http-not-found+)
    (update-cache)))

(restas:define-route playlist-editor/delete ("music/playlist/delete" :method :post)
  (:additional-variables (id (hunchentoot:post-parameter "id"))
                         (playlist (hunchentoot:post-parameter "playlist")))
  (:sift-variables (id 'integer))
  (prog1
    (if (delete-track (parse-integer id) (get-playlist playlist))
      hunchentoot:+http-ok+
      hunchentoot:+http-not-found+)
    (update-cache)))

(restas:define-route playlist-editor/move ("music/playlist/move" :method :post)
  (:additional-variables (id (hunchentoot:post-parameter "id"))
                         (delta (hunchentoot:post-parameter "delta"))
                         (playlist (hunchentoot:post-parameter "playlist")))
  (prog1
    (if (move-track (parse-integer id) (parse-integer delta) (get-playlist playlist))
      hunchentoot:+http-ok+
      hunchentoot:+http-not-found+)
    (update-cache)))

(restas:define-route playlist-editor/add-playlist ("music/playlist/add-playlist" :method :post)
  (:additional-variables (id (hunchentoot:post-parameter "id")))
  (add-playlist id)
  hunchentoot:+http-ok+)

(restas:define-route playlist-editor/delete-playlist ("music/playlist/delete-playlist"
                                                      :method :post)
  (:additional-variables (id (hunchentoot:post-parameter "id")))
  (remove-playlist id)
  hunchentoot:+http-ok+)

(restas:define-route playlist-editor/change-playlist ("music/playlist/change-playlist"
                                                      :method :post)
  (:additional-variables (id (hunchentoot:post-parameter "id")))
  (change-playlist id)
  hunchentoot:+http-ok+)

(restas:define-route playlist/id ("music/playlist/get-tracks/:id")
  (:apply-render-method #'hctsmsl:compile-html-forms)
  (if-let (playlist (get-playlist id))
    (render-playlist playlist)
    hunchentoot:+http-not-found+))

(restas:define-route playlist ("music/playlist")
  (:additional-variables (playlist (hunchentoot:post-parameter "playlist")))
  (:apply-render-method #'hctsmsl:compile-html-forms)
  (render-playlist (get-playlist (or playlist *default-playlist-id*))))

;; ----------------
;; Веб-интерфейс

(defun handle-file-upload (file)
  (with-input-from-string (stream file)
    (load-playlist (read-playlist stream))))

(restas:define-route playlist-file-upload ("music/playlist/load-file")
  (:additional-variables (file (hunchentoot:post-parameter "file")))
  (:apply-render-method #'hctsmsl:compile-html-forms)
  (bt:make-thread (curry #'handle-file-upload file)
                  :name "Playlist upload handler")
  (restas:redirect 'playlist-control-panel))

(restas:define-route playlist-control-panel ("music/control-center")
  (hctsmsl:html
    (<:doctype :html)
    (<:html
      (<:head
        (<:title "Teh Контрол Панэл")
        (<:meta '(:chatset "utf-8"))
        (<:link (list :rel "stylesheet"
                      :type "text/css"
                      :href (restas:genurl 'stylesheet))))
      (<:body
        (<:div '(:id "playlist-selection")
               (render-playlist-selector))
        (<:div '(:id "playlist-control"
                 :class "control")
               (<:div '(:class "track-addition")
                       (<:input '(:type "text"
                                  :id "track-title"
                                  :name "track-title"))
                       (<:button '(:id "add-track")
                                 "Добавить")
                       (<:form (list :class "playlist-upload"
                                     :method "post"
                                     :action (restas:genurl 'playlist-file-upload))
                               (<:input '(:type "file"
                                          :name "file"
                                          :id "file")
                                        "Загрузить файл")))
               (<:div/inline-close '(:id "request-status"
                                     :class "request-status"))
               (<:div '(:class "top-bar")
                      (make-playlist-control-toolbar))
               (<:table/block-close
                 '(:class "tracks-list")
                 (<:thead
                   (<:td/inline-close)
                   (<:td "Название")
                   (<:td "Исполнитель")
                   (<:td "Длительность")
                   (<:td "Качество (Кбит/с)")
                   (<:td "Сервис")
                   (<:td "Действия"))
                 (<:tbody/block '(:id "tracks"))))
        (<:script/inline-close (list :src (restas:genurl 'jquery)
                                     :type "text/javascript"))
        (<:script/inline-close (list :src (restas:genurl 'javascript)
                                     :type "text/javascript"))))))
