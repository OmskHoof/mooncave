(restas:define-module :mooncave.music
  (:use :cl
        :alexandria
        :mooncave)
  (:export :*playlists*            ; Dynamic variable
           :playlist-control-panel ; RESTAS route symbol
           :define-api             ; Macro
           :render-track           ; Generic function
           :Track                  ; Class
           :id                     ; Slot
           :service-id             ; Slot
           :title                  ; Slot
           :artist                 ; Slot
           :length                 ; Slot
           :quality                ; Slot
           :cached?                ; Slot
           :saved-pathname         ; Slot
           ))

(in-package :mooncave.music)

;;; **************************************************************************
;;;  Некоторые установки
;;; **************************************************************************

(setf yason:*parse-object-as* :alist)

;;; **************************************************************************
;;;  Методы
;;; **************************************************************************

(defgeneric render-track (track)
  (:documentation "В связи с разностью представления треков для каждого
сервиса, требуются следующие поля в формате списка в следующем порядке:
* Название трека/Имя файла
* Автор трека
* Длительность
* Битрейт
* Сервис
В случае, если какое-либо из полей отсутствует, возвращается длинное тире
(U+2014,—) или NIL."))
