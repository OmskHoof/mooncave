(in-package :mooncave.music)

;;; **************************************************************************
;;;  ФС
;;; **************************************************************************

(defun write-byte-stream-into-file (stream pathname
                                    &key (if-exists :error) if-does-not-exist)
  (with-open-file (file-stream pathname
                               :direction :output
                               :if-exists if-exists
                               :if-does-not-exist if-does-not-exist
                               :element-type '(unsigned-byte 8))
    (loop
      for byte = (read-byte stream nil nil)
      while byte
      do (write-byte byte file-stream))))

;;; **************************************************************************
;;;  XML DOM
;;; **************************************************************************

(defun get-dom-child-by-attribute (elt attribute value)
  (find-if #'(lambda (elt)
               (and (not (dom:text-node-p elt))
                    (equal (dom:get-attribute elt attribute)
                           value)))
           (dom:child-nodes elt)))

(defun get-dom-child-by-name (elt name)
  (find-if #'(lambda (elt)
               (and (not (dom:text-node-p elt))
                    (equal name
                           (dom:tag-name elt))))
           (dom:child-nodes elt)))

;;; **************************************************************************
;;;  JSON
;;; **************************************************************************

(defun json-val (value json-object)
  (cdr (assoc value json-object
              :test #'equalp)))

;;; **************************************************************************
;;;  Прочее
;;; **************************************************************************

(defun prepare-parameters-plist (parameters)
  (loop for (key value) on parameters by #'cddr
        collecting (cons (string-downcase (symbol-name key))
                         (typecase value
                           (symbol
                             (string-downcase (symbol-name value)))
                           (number
                             (write-to-string value))
                           (t
                             value)))))

(defun encode-time (time)
  (when time
    (format nil "~2,'0D:~2,'0D:~2,'0D"
            (floor (/ time 3600))
            (floor (/ time 60))
            (mod time 60))))

(defun nremove-n (n list)
  (check-type n non-negative-integer "A non-negative integer")
  (check-type list cons "A list")
  (when (<= (length list) n)
    (error "N can't be bigger than the length of the list."))
  (cond
    ((zerop n)
     (setf list (cdr list)))
    ((= n 1)
     (setf (cdr list) (cddr list)))
    ((> n 1)
     (nremove-n (1- n) (cdr list))))
  list)
