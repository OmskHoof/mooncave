(in-package :mooncave.music.icecast)

;;; **************************************************************************
;;;  Вспомогательные функции
;;; **************************************************************************

(defun get-mount (id)
  (cdr (assoc id *mounts*)))

;;; **************************************************************************
;;;  Методы доступа к данным
;;; **************************************************************************

(defun api-request (command &rest args)
  (declare (ignore args))
  (ecase command
    (:stats (get-icecast-stats))))

(defun get-icecast-stats ()
  (cxml:parse
    (drakma:http-request *api-url*
                         :basic-authorization (list *login* *password*))
    (cxml-dom:make-dom-builder)))

(defun get-current-song (&optional (mount-id :default))
  (let* ((stats (dom:document-element (get-icecast-stats)))
         (radio-source-data (get-dom-child-by-attribute stats "mount" (get-mount mount-id)))
         (title (dom:node-value (elt (dom:child-nodes
                                       (get-dom-child-by-name radio-source-data "title"))
                                     0)))
         (artist (dom:node-value (elt (dom:child-nodes
                                        (get-dom-child-by-name radio-source-data "artist"))
                                      0))))
    (list artist title)))
