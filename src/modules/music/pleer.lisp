(in-package :mooncave.music.pleer)

;;; ********************************************************************
;;;  API (основа)
;;; ********************************************************************

(defvar *token*            nil)
(defvar *token-issue-time* nil)

;; ----------------
;; MOP

(defclass Track (mooncave.music:Track)
  ((lyrics :initarg :lyrics)
   (size :initarg :size)))

(defun json->track (track)
  (make-instance 'Track
                 :service "Pleer"
                 :service-id (json-val "id" track)
                 :title (json-val "track" track)
                 :artist (json-val "artist" track)
                 :length (parse-integer (or (json-val "length" track)
                                            (json-val "lenght" track)))
                 :quality (cons :mp3 (json-val "bitrate" track))
                 :size (json-val "size" track)))

;; ----------------
;; HTTP

(defun token-valid-p (token issue-time)
  (and token
       issue-time
       (local-time:timestamp< (local-time:now)
                              (local-time:timestamp+ issue-time
                                                     *token-validness-time*
                                                     :hour))))

(defun get-new-token ()
  (let* ((parameters (list (cons "grant_type" "client_credentials")))
         (auth (list *api-id* *api-key*))
         (response (drakma:http-request *token-url*
                                        :method :post
                                        :parameters parameters
                                        :basic-authorization auth
                                        :external-format-in :utf-8
                                        :external-format-out :utf-8))
         (transformed-response (flexi-streams:octets-to-string response))
         (parsed-response (yason:parse transformed-response)))
    (json-val "access_token" parsed-response)))

(defun get-token ()
  (if (token-valid-p *token* *token-issue-time*)
    *token*
    (setf *token-issue-time* (local-time:now)
          *token*            (get-new-token))))

(defun api-request (method &rest parameters)
  (json-api-request *api-url*
                    (list* (cons "access_token" (get-token))
                           (cons "method" method)
                           (prepare-parameters-plist parameters))))

;;; **************************************************************************
;;;  API (Методы)
;;; **************************************************************************

(defun get-track-metadata (id)
  (json->track (json-val "data" (api-request "tracks_get_info" :track_id id))))

(defun get-track-lyrics (id)
  (json-val "text" (api-request "tracks_get_lyrics" :track_id id)))

(defun search-tracks (query &key (page "") (results-on-page "") (quality ""))
  (let* ((result (api-request "tracks_search"
                              :query query
                              :page page
                              :result_on_page results-on-page
                              :quality quality))
         (tracks (json-val "tracks" result)))
    (loop
      for (field . track) in tracks
      collecting (json->track track))))

(defun get-track-download-url (track reason)
  (when track
    (with-slots (service-id) track
      (json-val "url"
                (api-request "tracks_get_download_link"
                             :track_id service-id
                             :reason reason)))))

(defun get-top-list (type page language)
  (json-val "tracks"
            (api-request "get_top_list"
                         :list_type (ecase type
                                      (:week "1")
                                      (:month "2")
                                      (:3-months "3")
                                      (:halfyear "4")
                                      (:year "5"))
                         :page page
                         :language language)))

(defun get-autocompletion (string)
  (api-request "get_suggest"
               :part string))

;;; **************************************************************************
;;;  API модулей
;;; **************************************************************************

(defmethod cache-track ((id string))
  (when-let (track (get-track-metadata id))
    (cache-track track)))

(defmethod cache-track ((track Track))
  (cons (get-track-download-url track :save)
        track))

(defmethod find-track or (string &rest options)
  (first (search-tracks string
                        :results-on-page 1
                        :quality (getf options :quality ""))))
