(in-package :mooncave.music.lastfm)

;;; **************************************************************************
;;;  API (Основа)
;;; **************************************************************************

(defclass Track (mooncave.music:Track)
  ((mbid :initarg :mbid)
   (url :initarg :url)
   (listeners :initarg :listeners)
   (playcount :initarg :playcount)
   (artist-mbid :initarg :artist-mbid)
   (artist-url :initarg :artist-url)
   (album :initarg :album)
   (album-artist :initarg :album-aritst)
   (album-mbid :initarg :album-mbid)
   (album-url :initarg :album-url)
   (album-images :initarg :album-images)
   (tags :initarg :tags)))

(defun json->track (json)
  (let* ((track (json-val "track" json))
         (album (json-val "album" json))
         (artist (json-val "artist" json))
         (images (loop for image in (json-val "image" album)
                       for size = (make-keyword (string-upcase (json-val "size" image)))
                       for url = (json-val "#text" image)
                       appending (list size url)))
         (tags (loop for image in (json-val "toptags" album)
                     for name = (json-val "name" image)
                     for url = (json-val "url" image)
                     collecting (cons name url))))
    (make-instance 'Track
                   :service "LastFM"
                   :service-id (json-val "id" track)
                   :mbid (json-val "mbid" track)
                   :listeners (json-val "listeners" track)
                   :playcount (json-val "playcount" track)
                   :title (json-val "name" track)
                   :artist (json-val "name" artist)
                   :artist-mbid (json-val "mbid" artist)
                   :artist-url (json-val "url" artist)
                   :length (parse-integer (json-val "duration" track))
                   :url (json-val "url" track)
                   :album (json-val "title" album)
                   :album-aritst (json-val "artist" album)
                   :album-mbid (json-val "mbid" album)
                   :album-url (json-val "url" album)
                   :album-images images
                   :tags tags)))

(defun api-request (method &rest parameters)
  (json-api-request *api-url*
                    (list* (cons "api_key"     *api-key*)
                           (cons "method"      method)
                           (cons "format"      "json")
                           (prepare-parameters-plist parameters))))

;;; **************************************************************************
;;;  API (Высокоуровневые функции)
;;; **************************************************************************

(defun get-track-metadata (title artist)
  (let ((response (api-request "track.getinfo"
                               :track title
                               :artist artist
                               :autocorrect (if *autocorrect?*
                                              "1"
                                              "0"))))
    (get-dom-child-by-name (dom:document-element response) "track")))

(defun get-track-cover-url (metadata)
  (let* ((album (and metadata (get-dom-child-by-name metadata "album")))
         (url (and album (get-dom-child-by-attribute album "size" "large"))))
    (when url
      (dom:node-value (elt (dom:child-nodes url) 0)))))

(defun api-get-track-title (metadata)
  (let ((title (and metadata (get-dom-child-by-name metadata "title"))))
    (dom:node-value (elt (dom:child-nodes title) 0))))

;;; **************************************************************************
;;;  API модулей
;;; **************************************************************************

(defmethod find-track or (string &rest options)
  (declare (ignore options))
  (destructuring-bind (artist title) (cl-ppcre:split " (-|‒|–|—) " string)
    (get-track-metadata title artist)))
