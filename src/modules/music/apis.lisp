;;; **************************************************************************
;;;  МАКРОСЫ!
;;; **************************************************************************

(in-package :mooncave.music)

(defmacro define-api (name (&rest additional-use) &body methods)
  `(eval-when (:load-toplevel :compile-toplevel :execute)
     (defpackage ,name
       (:use :cl
             :alexandria
             ,@additional-use)
       (:import-from :mooncave.music
                     :json-val                   ; Function
                     :prepare-parameters-plist   ; Function
                     :encode-time                ; Function
                     :get-dom-child-by-attribute ; Function
                     :get-dom-child-by-name      ; Function
                     :json-api-request           ; Function
                     :render-track               ; Generic function
                     :cache-track                ; Generic function
                     :find-track                 ; Generic function
                     :id                         ; Slot
                     :service-id                 ; Slot
                     :title                      ; Slot
                     :artist                     ; Slot
                     :length                     ; Slot
                     :quality                    ; Slot
                     :cached?                    ; Slot
                     :saved-pathname             ; Slot
                     :api-error                  ; Condition
                     )
       (:export :api-request                     ; Function
                :cache-track                     ; Function
                :Track                           ; Class
                ,@methods))))

;;; **************************************************************************
;;;  Разное
;;; **************************************************************************

(define-condition API-error () ())

(defun json-api-request (url parameters)
  (multiple-value-bind (response response-code)
      (drakma:http-request url
                           :method :post
                           :external-format-in :utf-8
                           :external-format-out :utf-8
                           :parameters parameters)
    (if (not (= response-code 200))
      (error 'API-error)
      (typecase response
        (string (yason:parse response))
        (vector (yason:parse (flexi-streams:octets-to-string response)))))))

;;; **************************************************************************
;;;  Icecast
;;; **************************************************************************

(mooncave.music:define-api :mooncave.music.icecast ()
  :get-icecast-stats
  :get-current-song)

(in-package :mooncave.music.icecast)

(defparameter *api-url*     "http://127.0.0.1:8000/admin/stats")
(defparameter *mounts*      '((:default     . "/radio")
                              (:low-quality . "/radio64")))

;;; **************************************************************************
;;;  LastFM
;;; **************************************************************************

(mooncave.music:define-api :mooncave.music.lastfm ()
  :api-request
  :get-track-metadata
  :get-track-cover-url
  :get-track-title)

(in-package :mooncave.music.lastfm)

(defparameter *api-url*      "http://ws.audioscrobbler.com/2.0/")
(defparameter *autocorrect?* t)

;;; **************************************************************************
;;;  Jamendo
;;; **************************************************************************

(mooncave.music:define-api :mooncave.music.jamendo ()
  ;; Пока что пусто
  )

;;; **************************************************************************
;;;  Magnatune
;;; **************************************************************************

;;; Низкий приоритет, потому что Jamendo, VK и ПростоПлеера, скорее всего, будет
;;; достаточно на первое время.

(mooncave.music:define-api :mooncave.music.magnatune ()
  ;; Пока что пусто.
  )

;;; **************************************************************************
;;;  Pleer
;;; **************************************************************************

(mooncave.music:define-api :mooncave.music.pleer ()
  :api-request
  :search-tracks
  :get-track-metadata
  :get-track-lyrics
  :get-track-download-url
  :get-top-list
  :get-autocompletion)

(in-package :mooncave.music.pleer)

(defparameter *api-url*   "http://api.pleer.com/index.php")
(defparameter *token-url* "http://api.pleer.com/token.php")
(defparameter *token-validness-time* 1
  "В часах.")

;;; **************************************************************************
;;;  ВеКа
;;; **************************************************************************

(mooncave.music:define-api :mooncave.music.vk ()
  ;; Пока что пусто.
  )
