(in-package :mooncave.music)

(defparameter *cache-pathname* #p"/tmp/radio-tracks-cache/cached-track")

;;; **************************************************************************
;;;  Generic-функции
;;; **************************************************************************

(defgeneric cache-track (track)
  (:documentation "Помещает трэк в кэш. В кэше может быть только один трек.
Текущее содержимое кэша выкидывается на помойку.
Методы для API должны возвращать список вида (download-url track-instance)."))

(defmethod cache-track :around (track)
  (destructuring-bind (url . metadata) (call-next-method track)
    (declare (ignore metadata))
    (cache-url url)))

;;; **************************************************************************
;;;  Загрузка файлов
;;; **************************************************************************

(defun cache-url (url)
  (ensure-directories-exist *cache-pathname*)
  (write-byte-stream-into-file (drakma:http-request url :want-stream t)
                               *cache-pathname*
                               :if-exists :supersede
                               :if-does-not-exist :create)
  *cache-pathname*)

;;; **************************************************************************
;;;  Получение данных из кэша
;;; **************************************************************************

(defun get-cached-track ()
  *cache-pathname*)

;;; **************************************************************************
;;;  Обновление кэша
;;; **************************************************************************

(defun cache-updater-thread ()
  (when-let (next-track (current-track (current-playlist) 1))
    (unless (track-id= (track-id next-track)
                       (track-id (current-track (current-playlist))))
      (cache-track next-track))))

(defun update-cache ()
  (bt:make-thread #'cache-updater-thread
                  :name "Cache updater thread"))
