(in-package :mooncave.music)

(defparameter *playlists-save-pathname* (resource "playlists.db"))

(defparameter *default-playlist-id* "default")

(defvar *playlists* nil)
(defvar *current-playlist* nil)

(defvar *next-track-id* 0)

;;; **************************************************************************
;;;  Классы
;;; **************************************************************************

(defun gen-track-id ()
  (prog1 *next-track-id*
    (incf *next-track-id*)))

(defclass Track ()
  ((id :initarg :id
       :initform (gen-track-id)
       :reader track-id)
   (service :initarg :service
            :reader track-service)
   (service-id :initarg :service-id
               :documentation "Внутренний ID сервиса, предоставляющего песню."
               :reader track-service-id)
   (title :initarg :title
          :reader track-title)
   (artist :initarg :artist
           :reader track-artist)
   (length :initarg :length
           :reader track-length)
   (quality :initarg :quality
            :reader track-quality)
   (saved-pathname :initarg :saved-pathname
                   :initform nil
                   :documentation "Путь к сохранённому файлу. Если не сохранён, NIL."
                   :accessor track-saved-pathname)))

(defmethod print-object ((track Track) stream)
  (print-unreadable-object (track stream :type t)
    (with-slots (id service-id artist title length) track
      (format stream "[~A] \"~A\" \"~A\" {~A | ~A}"
              (encode-time length)
              artist title id service-id))))

(defclass Playlist ()
  ((id :initarg :id
       :accessor playlist-id)
   (description :initarg :description
                :accessor playlist-description)
   (position :initarg :position
             :initform 0
             :accessor playlist-position)
   (position-lock :initform (bt:make-lock "Playlist position"))
   (items :initarg :items
          :initform ()
          :accessor playlist-items)
   (items-lock :initform (bt:make-lock "Playlist items"))))

;;; **************************************************************************
;;;  ФС
;;; **************************************************************************

(defun save-playlists ()
  (save-data *next-track-id* "next-track-id")
  (save-data *playlists* "playlists")
  (save-data *current-playlist* "current-playlist")
  t)

(defun load-playlists ()
  (setf *next-track-id*    (load-data "next-track-id")
        *playlists*        (load-data "playlists")
        *current-playlist* (load-data "current-playlist"))
  t)

(defun sync-playlists ()
  ;; Так как пока что допускается запуск только одной копии бэкэнда, достаточно
  ;; просто свалить все изменения на постоянное хранение.
  (save-playlists))

;;; **************************************************************************
;;;  Трэки
;;; **************************************************************************

(defun track-id= (id-1 id-2)
  (= id-1 id-2))

(define-condition Track-not-found ()
  ((query :initarg :query)))

(defgeneric find-track (string &rest options)
  (:documentation "Ищет трек по всем доступным сервисам.")
  (:method-combination or))

(defmethod find-track or (string &rest options)
  (declare (ignore options))
  (error 'Track-not-found
         :query string))

;;; **************************************************************************
;;;  Плейлист
;;; **************************************************************************

(defun make-playlist (id description &rest initial-items)
  (make-instance 'Playlist
                 :id id
                 :description description
                 :items initial-items))

(defmacro with-locked-playlist ((playlist) &body body)
  (with-gensyms (items-lock position-lock)
    `(with-slots ((,items-lock items-lock) (,position-lock position-lock))
         ,playlist
       (bt:with-lock-held (,items-lock)
         (bt:with-lock-held (,position-lock)
           ,@body)))))

(defun push-track (playlist track)
  (with-locked-playlist (playlist)
    (push track (playlist-items playlist))
    track))

(defun pop-track (playlist)
  (with-locked-playlist (playlist)
    (pop-track (playlist-items playlist))))

(defun remove-track (playlist id)
  (with-locked-playlist (playlist)
    (remove id (playlist-items playlist)
            :test #'track-id=
            :key #'track-id)))

(defun next-track (playlist &optional (delta 1))
  (with-locked-playlist (playlist)
    (with-slots (items position) playlist
      (setf position (mod (+ position delta)
                          (let ((len (length items)))
                            (if (zerop len)
                              1
                              len)))))))

(defun previous-track (playlist &optional (delta 1))
  (with-locked-playlist (playlist)
    (next-track (playlist-position playlist) (- delta))))

(defun current-track (playlist &optional (shift 0))
  (with-locked-playlist (playlist)
    (with-slots (items position) playlist
      (nth (mod (+ position shift)
                (let ((len (length items)))
                            (if (zerop len)
                              1
                              len)))
           (playlist-items playlist)))))

;;; **************************************************************************
;;;  Плейлисты
;;; **************************************************************************

(defun playlist-exists-p (id)
  (when (get-playlist id)
    t))

(defun add-playlist (id &optional description)
  (unless (playlist-exists-p id)
    (let ((playlist (make-playlist id description)))
      (push playlist *playlists*)
      playlist)))

(defun remove-playlist (id)
  (setf *playlists* (remove id *playlists*
                            :key #'playlist-id
                            :test #'equalp)))

(defun get-playlist (id)
  (find (or id *default-playlist-id*) *playlists*
        :key #'playlist-id
        :test #'equalp))

(defun current-playlist ()
  (or *current-playlist*
      (setf *current-playlist* (or (get-playlist *default-playlist-id*)
                                   (add-playlist *default-playlist-id*)))))

(defun (setf current-playlist) (new-value)
  (setf *current-playlist* new-value))

(defun change-playlist (id)
  (setf (current-playlist) (get-playlist id)))

(defmacro with-playlist (playlist (items position) &body body)
  `(prog1
     (with-locked-playlist (,playlist)
       (with-slots ((,items items) (,position position)) ,playlist
         (declare (ignorable ,items ,position))
         ,@body))
     (sync-playlists)))

(defmacro with-current-playlist ((items position) &body body)
  `(with-playlist (current-playlist) (,items ,position)
     ,@body))

;;; **************************************************************************
;;;  События
;;; **************************************************************************

;; ----------------
;; Базовые классы

(defclass Event ()
  ((source :initarg :source)))

(defgeneric dispatch-event (event))

;; ----------------
;; События

(defclass Next-track (Event) ())

(defclass Playlist-change (Event)
  ((new-playlist :initarg :new-playlist)))

(defmethod dispatch-event ((event Next-track))
  (next-track (current-playlist))
  (update-cache))

(defmethod dispatch-event ((event Playlist-change))
  (with-slots (new-playlist) event
    (setf *current-playlist* (get-playlist new-playlist))))

;; ----------------
;; Сигнализация событий

(defun signal-event (type &rest initargs)
  (dispatch-event (apply #'make-instance type initargs)))

;; ----------------
;; Расписание

(defparameter *days-of-week*
  '((:monday    . 0)
    (:tuesday   . 1)
    (:wednesday . 2)
    (:thursday  . 3)
    (:friday    . 4)
    (:saturday  . 5)
    (:sunday    . 6)))

(defparameter *scheduler-run-interval* 60
  "В секундах. Максимальная погрешность — 1 секунда.")

(defvar *timetable* (make-hash-table :test 'equal))
(defvar *scheduler* nil)

(defun timetable-record (hour minute dow)
  (gethash (list hour minute dow) *timetable*))

(defun (setf timetable-record) (new-value hour minute &optional dow)
  (prog1
    (cond
      ((null dow)
       (dolist (dow *days-of-week*)
         (setf (gethash (list hour minute (cdr dow)) *timetable*) new-value)))
      ((symbolp dow)
       (if-let (dow (cdr (assoc dow *days-of-week*)))
         (setf (gethash (list hour minute dow) *timetable*) new-value)
         (error "Unknown day of week: ~S" dow)))
      (t
       (setf (gethash (list hour minute dow) *timetable*) new-value)))
    (sync-timetable)))

(defun remove-timetable-record (hour minute &optional dow)
  (prog1
    (cond
      ((null dow)
       (dolist (dow *days-of-week*)
         (remhash (list hour minute (cdr dow)) *timetable*)))
      ((symbolp dow)
       (if-let (dow (cdr (assoc dow *days-of-week*)))
         (remhash (list hour minute dow) *timetable*)
         (error "Unknown day of week: ~S" dow)))
      (t
       (remhash (list hour minute dow) *timetable*)))
    (sync-timetable)))

(defun clear-timetable ()
  (prog1
    (clrhash *timetable*)
    (sync-timetable)))

(defun print-timetable ()
  (maphash #'(lambda (key value)
               (destructuring-bind (hour minute dow) key
                 (format t "~2,'0D:~2,'0D ~D | ~A~%" hour minute dow value)))
           *timetable*)
  (values))

(define-condition stop-thread () ())

(defun make-scheduler-function (&optional (interval *scheduler-run-interval*))
  #'(lambda ()
      (let ((running? t))
        (handler-bind
          ((stop-thread #'(lambda (condition)
                            (declare (ignore condition))
                            (setf running? nil))))
          (loop
            while running?
            for dt = (- interval
                        (nth-value 1
                                   (truncate (get-universal-time)
                                             interval)))
            doing
            (sleep dt)
            (when (hash-table-p *timetable*)
              (multiple-value-bind (sec min hour day month year dow) (get-decoded-time)
                (declare (ignore sec day month year))
                (when-let (record (timetable-record hour min dow))
                  (apply #'signal-event (car record) (cdr record))))))))))

(defun start-scheduler ()
  (unless (and (bt:threadp *scheduler*) (bt:thread-alive-p *scheduler*))
    (setf *scheduler* (bt:make-thread (make-scheduler-function) :name "Task scheduler"))))

(defun stop-scheduler ()
  (when (and (bt:threadp *scheduler*) (bt:thread-alive-p *scheduler*))
    (bt:interrupt-thread *scheduler* #'(lambda () (signal 'stop-thread)))))

(defun save-timetable ()
  (save-data *timetable* "timetable"))

(defun load-timetable ()
  (setf *timetable* (load-data "timetable")))

(defun sync-timetable ()
  (save-timetable))

;; По UTC+0300.
;; 02:00 — 06:00 : Брони-музыка 1;
;; 06:00 — 07:00 : Бодряк!;
;; 07:00 — 17:00 : Брони-музыка 1;
;; 17:00 — 18:00 : Рок и лёгкий металл;
;; 18:00 — 02:00 : Тяжёлый метал;

;; (add-playlist "thematic")
;; (add-playlist "morning")
;; (add-playlist "rock")
;; (add-playlist "heavy-metal")
;;
;; ;; По UTC+0000
;; (setf (timetable-record 23 0) '(playlist-change :new-playlist "thematic")
;;       (timetable-record 3 0)  '(playlist-change :new-playlist "morning")
;;       (timetable-record 4 0)  '(playlist-change :new-playlist "thematic")
;;       (timetable-record 14 0) '(playlist-change :new-playlist "rock")
;;       (timetable-record 15 0) '(playlist-change :new-playlist "heavy-metal"))

;;; **************************************************************************
;;;  Парсер текстового формата
;;; **************************************************************************

;; Формат:
;;
;; @<Название> (<комментарий>) [00:00-11:00,12:00-14:00@Mon,Tue,Wed]
;; !<Первый трек>
;; <Остальные треки>
;;
;; Название может содержать только латиницу, цифры, дефис и символ нижнего подчёркивания.
;;
;; Формат трека:
;; * #service:ID — поиск по айди на серсисе service.
;; * Artist − Ttitle — поиск по строке по всем сервисам. Наиболее близкая добавляется в список.
;;
;; На данный момент доступны следующие сервисы:
;; * pleer
;;
;; Для обозначения всех дней недели может быть использован астериск: [00:00-23:00@*]
;; Остальные треки могут быть перемешаны. Пропуски строк не учитываются.

(defparameter *header-regex* "@([a-zA-Z0-1-_]+) \\(([^\\(\\)]*)\\) \\[([0-9]{2}:[0-9]{2}-[0-9]{2}:[0-9]{2}(?:,[0-9]{2}:[0-9]{2}-[0-9]{2}:[0-9]{2})*)@([^\\[\\]]*)\]$")

(defparameter *track-regex* "^[!]?([^−]+) — ([^−]+)$")

(defparameter *time-interval-regex* "([0-9]{2}):([0-9]{2})-([0-9]{2}):([0-9]{2})")

(defparameter *nasty-characters* #.(coerce '(#\Newline #\Return #\Space) 'string))

(defparameter *request-timeout* 1)

(define-condition Incorrect-line ()
  ((line :initarg :line)
   (message :initarg :message)))

(defun parse-time-interval (string)
  (mapcar #'parse-integer
          (coerce (nth-value 1 (ppcre:scan-to-strings *time-interval-regex* string))
                  'list)))

(defun invert-dows (list)
  (set-difference (mapcar #'car *days-of-week*) list))

(defun parse-dow (string)
  (switch (string :test #'equalp)
    ("Mon" :monday)
    ("Tue" :tuesday)
    ("Wed" :wednesday)
    ("Thu" :thursday)
    ("Fri" :friday)
    ("Sat" :saturday)
    ("Sun" :sunday)))

(defun parse-dows (string)
  (cond
    ((equal "*" string)
     nil)
    ((equal #\! (char string 0))
     (invert-dows (parse-dows (subseq string 1))))
    (t
     (mapcar #'parse-dow (cl-ppcre:split "," string)))))

(defun parse-header (string)
  (or (when-let ((parsed (nth-value 1 (cl-ppcre:scan-to-strings *header-regex* string))))
        (destructuring-bind (id description schedule dows)
            (coerce parsed 'list)
          (list :id (intern (string-upcase id))
                :description description
                :schedule (mapcar #'parse-time-interval (ppcre:split "," schedule))
                :dows (parse-dows dows))))
      (error 'Incorrect-line
             :line string
             :message "Не соответствует формату заголовка.")))

(defun parse-track (string)
  (if-let ((parsed (nth-value 1 (cl-ppcre:scan-to-strings *track-regex* string))))
    (values (coerce parsed 'list)
            (char= #\! (elt string 0)))
    (error 'Incorrect-line
           :line string
           :message "Не соответствует формату трека.")))

(defun read-header (stream)
  (if-let (string (read-line stream nil nil))
    (parse-header (string-trim *nasty-characters* string))))

(defun find-parsed-track (parsed)
  (when parsed
    (let ((query (format nil "~A - ~A" (first parsed) (second parsed))))
      (handler-case (find-track query)
        (API-error () nil)
        (Track-not-found () nil)))))

(defun read-tracks (stream)
  (loop for line = (read-line stream nil nil)
        while line
        for clean-line = (string-trim *nasty-characters* line)
        for track = (find-parsed-track (when (< 0 (length clean-line))
                                         (parse-track clean-line)))
        when track collect track))

(defun read-playlist (stream &optional (skip 0))
  (let ((header (read-header stream)))
    (loop repeat skip do (read-line stream))
    (list* header (read-tracks stream))))

(defun parse-playlist (pathname &optional (skip 0))
  (with-open-file (stream pathname :direction :input :if-does-not-exist :error)
    (read-playlist stream skip)))

(defun load-playlist (playlist)
  (destructuring-bind (header . tracks) playlist
    (let* ((id (getf header :id))
           (description (getf header :description))
           (schedule (getf header :schedule))
           (dows (getf header :dows))
           (playlist (make-playlist id description tracks)))
      (loop for dow in dows
            for (hour-start minute-start hour-end minute-end) in schedule
            doing (setf (timetable-record hour-start minute-start dow) playlist)))))
