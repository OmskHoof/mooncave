(in-package :mooncave.metadata-updater)

;;; **************************************************************************
;;;  Новый API
;;; **************************************************************************

(restas:define-route track-cover ("track/cover" :content-type "image/png")
  (:decorators 'mooncave:@no-cache)
  (or *song-cover* *default-cover*))

(restas:define-route track-title ("track/title")
  (:decorators 'mooncave:@no-cache)
  (hctsmsl:html
    (<:a
      (<:b
        (if (emptyp *song-artist*)
          "Неизвестный Исполнитель"
          *song-artist*))
      (unless (emptyp *song-artist*)
        " – ")
      (if (emptyp *song-title*)
        "Трек #1"
        *song-title*))))
