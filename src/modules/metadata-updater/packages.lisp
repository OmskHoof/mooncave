(restas:define-module :mooncave.metadata-updater
  (:use :cl
        :alexandria
        :mooncave)
  (:export :metadata

           :*default-cover*
           :*default-title*
           :*default-artist*

           :*icecast-url*
           :*icecast-login*
           :*icecast-password*
           :*icecast-mount*
           :*icecast-song-regex*

           :*song-cache-update-interval*

           :*song-full-metadata*
           :*song-cover*
           :*song-title*
           :*song-artist*

           :track-cover
           :track-title))

(in-package :mooncave.metadata-updater)

(defparameter *default-cover* (static-file #p"img/no-cover.png"))
(defparameter *default-title*  "Трек #1")
(defparameter *default-artist* "Неизвестный Исполнитель")

(defparameter *song-cache-update-interval* 10
  "В секундах.")
