(in-package :mooncave.metadata-updater)

(defvar *song-full-metadata* nil)
(defvar *song-cover* *default-cover*)
(defvar *song-title* nil)
(defvar *song-artist* nil)

(defun get-song-cover (metadata)
  (let ((url (mooncave.music.lastfm:get-track-cover-url metadata)))
    (when url
      (drakma:http-request url))))

(defun cache-current-song-metadata ()
  (let ((song (mooncave.music.icecast:get-current-song)))
    (destructuring-bind (artist title) song
      (if (and (equal "" artist) (equal "Unknown" title))
        (setf *song-cover* *default-cover*
              *song-title* *default-title*
              *song-artist* *default-artist*
              *song-full-metadata* nil)
        (let ((metadata (mooncave.music.lastfm:get-track-metadata title artist)))
          (setf *song-cover* (get-song-cover metadata)
                *song-title* title
                *song-artist* artist
                *song-full-metadata* metadata)
          song)))))
