(in-package :mooncave.metadata-updater)

(defvar *cache-updater* nil)

(defun make-cache-updater (update-interval)
  #'(lambda ()
      (loop
        (handler-case (cache-current-song-metadata)
          (error () nil)) ; Да, да, я знаю, что это не ОК, но похуй.
                          ; TODO Сделать логгирование ошибок.
        (sleep update-interval))))

(defun start-cache-updater (update-interval)
  "Побочные эффекты!"
  (unless (and *cache-updater* (bt:thread-alive-p *cache-updater*))
    (setf *cache-updater* (bt:make-thread (make-cache-updater update-interval)
                                          :name "Song metadata cache updater"))))

(defun stop-cache-updater ()
  "Побочные эффекты!"
  (when (and *cache-updater* (bt:thread-alive-p *cache-updater*))
    (bt:destroy-thread *cache-updater*)))

(defmethod restas:initialize-module-instance :before ((module (eql #.*package*)) context)
  (declare (ignore context))
  (start-cache-updater *song-cache-update-interval*))

;; Отрубил, потому что оно только проблемы создаёт. Нахуя, спрашивается, тогда оно нужно?
;; Может когда-нибудь и врублю обратно. Оставляю код на случай если кому-то надо будет врубить
;; или мне приспичит на продакшене тестировать.
#+nil
(defmethod restas:finalize-module-instance :after ((module (eql #.*package*)) context)
  (declare (ignore context))
  (stop-cache-updater))
